var ss = require('socket.io-stream');
var magic = require('stream-mmmagic');

var mime = require('mime');

module.exports = function(note, config, tools, util, io, fs, Sequelize, models, request, prove) {
	io = io.of('/create');
	io.on('connection', function(socket) {
		var userIp = (socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address);
		note('003', '001', 'Socket connection established (create, \'' + userIp + '\')');

		var uploaded;

		// File upload event
		ss(socket).on('file', function(stream, data) {
			// Detect file type
			magic(stream, function(error, mimedata, output) {
				var size = 0;

				if(error) {
					note('004', '003', error);
				} else {
					// Generate unique ID for database look-up and to prevent filename collisions
					var id = tools.id('topic');
					uploaded = id;

					var fileformat = mime.lookup(data.name).split('/')[1];
					var mimetype = mimedata.type.split('/')[0];
					var mimeformat = mimedata.type.split('/')[1];

					if((config.upload.valid[mimetype] || new Array()).indexOf(mimeformat) > -1 && mimeformat == fileformat) {
						// Generate unique filename to prevent filename collisions
						var filepath = config.upload.path[mimetype] + tools.id('file') + '.' + mime.extension(mimedata.type);

						output.pipe(fs.createWriteStream(filepath));

						stream.on('data', function(chunk) {
							size += chunk.length;

							if(data.size > config.upload.maxSize[mimetype] || size > config.upload.maxSize[mimetype]) {
								stream.end();

								io.to(socket.id).emit('uploadFeedback', {'content': 'error', 'message': _.capitalize([mimetype]) + ' can\'t be larger than ' + Math.round(config.upload.maxSize[mimetype] / 1000) + 'kB!'});

								fs.unlink(filepath, function(error) {
									if(error) {
										note('004', '003', error);
									} else {
										note('004', '001', 'DEL: ' + filepath);
									}
								});
							} else {
								io.to(socket.id).emit('uploadFeedback', {'content': 'progress', 'progress': Math.round((size / data.size) * 100)});
							}
						});

						stream.on('end', function() {
							fs.stat(filepath, function(error, stats) {
								if(!error) {
									// './public' is removed from path as recipient is contained by './public'
									filepath = filepath.split('/').slice(2,filepath.length).join('/');
									note('004', '001', 'ADD: ' + filepath + ' (' + Math.round(size / 1000) + 'kB)');
									io.to(socket.id).emit('uploadFeedback', {'content': 'complete', 'id': id, 'url': config.upload.host + filepath, 'type': mimetype});
								}
							});
						});
					} else {
						note('004', '003', 'Invalid filetype: ' + mimedata.type);

						// Emit error event for invalid filetype
						io.to(socket.id).emit('uploadFeedback', {'content': 'error', 'message': 'Invalid filetype!'});
				 	}
				}
			});
		});

		socket.on('link', function(url) {
			request.get({'url': url, 'headers': {'User-Agent': 'Mozilla/5.0'}}, function(error, response, body) {
				if(error) {
					io.to(socket.id).emit('uploadFeedback', {'content': 'error', 'message': 'Invalid URL!'});
				} else if((response ? response.statusCode : false) !== 200) {
					io.to(socket.id).emit('uploadFeedback', {'content': 'error', 'message': 'File not found or invalid!'});
				} else if(config.create.embed[response.request.originalHost]) {
					note('001', '001', 'EMBED: ' + response.request.originalHost);

					// Generate unique ID for database look-up
					var id = tools.id('topic');
					uploaded = id;

					switch(config.create.embed[response.request.originalHost]) {
						case 'youtube':
							io.to(socket.id).emit('uploadFeedback', {'content': 'complete', 'id': id, 'url': url, 'type': 'youtube'});
							break;
						case 'soundcloud':
							io.to(socket.id).emit('uploadFeedback', {'content': 'complete', 'id': id, 'url': url, 'type': 'soundcloud'});
							break;
					}
				} else {
					var mimetype = response.headers['content-type'].split('/')[0];
					var mimeformat = response.headers['content-type'].split('/')[1];

					if(!config.upload.valid[mimetype]) {
						note('004', '003', 'Invalid filetype: ' + mimeformat);

						io.to(socket.id).emit('uploadFeedback', {'content': 'error', 'message': 'Invalid filetype!'});
					} else {
						// Generate unique ID for database look-up
						var id = tools.id('topic');
						uploaded = id;

						io.to(socket.id).emit('uploadFeedback', {'content': 'complete', 'id': id, 'url': url, 'type': mimetype});
					}
				}
			});
		});

		socket.on('create', function(data) {
			var validateTitle = prove()
				.required().annotate({'required': 'Please supply a title!'})
				.not.empty().annotate({'not.empty': 'Please supply a title!'})
				.length(config.create.title.minLength, config.create.title.maxLength).annotate({'length': 'Title is not long enough or too long!'})
				.test(data.title);

			// io.to(socket.id).emit('createFeedback', {'content': 'error', 'message': });
			if(!data.id || data.id !== uploaded) {
				io.to(socket.id).emit('feedback', {'content': 'error', 'field': 'upload', 'message': 'Please upload or link a file!'});
			} else if(validateTitle !== true) {
				note('004', '001', util.inspect(validateTitle));
				io.to(socket.id).emit('feedback', {'content': 'error', 'field': 'title', 'message': validateTitle.errors[0][validateTitle.errors[0].type]});
			} else if((data.tags ? data.tags.length : 0) < 1) {
				io.to(socket.id).emit('feedback', {'content': 'error', 'field': 'tags', 'message': 'Please supply at least one tag!'});
			} else {
				models.Topics
					.find({ where: { id: uploaded }})
					.then(function successCallback(id){
						if(!id) {
							models.Topics
								.create({
									id: data.id,
									title: data.title,
									type: data.type,
									url: data.url,
									tags: data.tags.toString()
								})
								.then(function successCallback(topic) {
									models.Rooms
										.create({
											id: tools.id('room'),
											topic: topic.dataValues.id,
											label: data.title,
											primary: 1
										})
										.then(function successCallback() {
											io.to(socket.id).emit('feedback', {'content': 'success', 'message': 'Upload successful!', 'id': topic.dataValues.id});
										}, function errorCallback(error) {
											note('002', '003', error);
											io.to(socket.id).emit('feedback', {'content': 'error', 'message': 'Could not create room for submission!'});
										});
								}, function errorCallback(error) {
									note('002', '003', error);
									io.to(socket.id).emit('feedback', {'content': 'error', 'message': 'Submission failed!'});
								});
						} else {
							io.to(socket.id).emit('feedback', {'content': 'error', 'message': 'Submission already exists!', 'id': uploaded});
						}
				}, function errorCallback(error){
					note('002', '003', error);
					io.to(socket.id).emit('feedback', {'content': 'error', 'message': 'Something went wrong!'});
				});
			}
		});

		socket.on('disconnect', function() {
			note('003', '001', 'Socket connection ended (topics, \'' + userIp + '\')');
		});
	});
}
