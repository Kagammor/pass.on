module.exports = function(note, config, tools, util, io, fs, Sequelize, models, request, prove) {
	io = io.of('/topics');
	io.on('connection', function(socket) {
		var userIp = (socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address);
		note('003', '001', 'Socket connection established (topics, \'' + userIp + '\')');
	
		models.Topics
			.findAll()
			.then(function successCallback(topics) {
				io.to(socket.id).emit('topics', topics);
			}, function errorCallback(error) {
				note('002', '003', error);
			});
		
		socket.on('disconnect', function() {
			note('003', '001', 'Socket connection ended (topics, \'' + userIp + '\')');
		});
	});
}
