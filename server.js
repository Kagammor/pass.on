// 3rd party modules
var Promise = require("bluebird");
var _ = require('lodash');

var io = require('socket.io').listen(3000);

var request = require('request');
var Sequelize = require('sequelize');

var fs = require('fs');
Promise.promisifyAll(fs);
var path = require('path');

var reflekt = require('reflekt');

var mime = require('mime');
var uuid = require('node-uuid');

var util = require('util');
var moment = require('moment');
var prove = require('prove-it');

// Custom modules
var note = require('./modules/note');

var config = require('./config.json');

prove.extend({
	conflict: function(collection) {
		return function(value) {
			var conflicts = collection.filter(function(item) {
				return value.toLowerCase() === item.toLowerCase();
			});
			
			return (conflicts.length > 0);
		}
	}
});

// Connect to database
var sequelize = new Sequelize(config.sequelize.database, config.sequelize.user, config.sequelize.password, { dialect: 'mysql', logging: (config.sequelize.logging ? function(msg){ note('002', '001', msg); } : false)});

// Check database connection status
sequelize
	.authenticate()
	.then(function successCallback(){
		note('002', '001', 'Connection authenticated', true);
	}, function errorCallback(err){
		note('002', '003', err.message);
	});

var models = {
	Topics: sequelize.define('Topic', {
		id: Sequelize.UUID,
		title: Sequelize.STRING,
		type: Sequelize.STRING,
		url: Sequelize.STRING,
		tags: Sequelize.STRING
	}),
	Rooms: sequelize.define('Room', {
		id: Sequelize.UUID,
		topic: Sequelize.UUID,
		label: Sequelize.STRING,
		primary: Sequelize.BOOLEAN
	})
}

sequelize
	.sync()
	.then(function successCallback(){
		note('002', '001', 'Synchronized data models', true);
	}, function errorCallback(error){
		note('002', '003', error.message);
	});

var passthrough = {
	note: note,
	config: config,
	util: util,
	io: io,
	Sequelize: Sequelize,
	models: models,
	request: request,
	fs: fs,
	path: path,
	prove: prove,
	uuid: uuid
}

passthrough['tools'] = reflekt.call(require('./tools.js'), passthrough);

reflekt.call(require('./create.js'), passthrough);
reflekt.call(require('./topics.js'), passthrough);
reflekt.call(require('./chat.js'), passthrough);
