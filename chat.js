var namegen = require('./modules/namegen');

module.exports = function(note, config, tools, util, io, fs, Sequelize, models, request, prove) {
	var users = new Object();

	io = io.of('/chat');
	io.on('connection', function(socket) {
	
		var syncData = new Object();
	
		var userIp = (socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address);
		note('003', '001', 'Socket connection established (chat, \'' + userIp + '\')');
	
		syncData.user = {
			'name': namegen(),
			'color': 'rgb(' + tools.hexgen() + ',' + tools.hexgen() + ',' + tools.hexgen() + ')',
			'status': 'available'
		}
		syncData.room = {
			'palette': config.defaults.colors
		};
	
		var blacklist = function(callback) {
			fs.readFile('./blacklist.json', function(error, data) {
				if(!error) {
					callback(JSON.parse(data));
				}
			});
		}
	
		blacklist(function(blacklist) {
			if(blacklist[userIp]) {
				note('001', '002', 'Banned user tried to join (IP: ' + userIp + ')');
			
				io.to(socket.id).emit('broadcast', {'msg': 'You have been banned.', 'personal': true});
				socket.disconnect();
			}
		});
	
		var appCommands = {
			'newNick': function(newNick) {
				// Check if user has joined a room
				if(tools.extract(socket.rooms, socket.id)) {
					var room = tools.extract(socket.rooms, socket.id);
			
					var validation = tools.validateNick(newNick, syncData.user.name, users);
				
					if(validation !== true) {
						// Retrieve specific validation error by calling annotation with failure type as key
						io.to(socket.id).emit('broadcast', {'msg': validation.errors[0][validation.errors[0].type], 'personal': true});
					} else {
						note('001', '001', "'" + syncData.user.name + "' changed username to '" + newNick + "'");
						io.to(room).emit('broadcast', {'msg': syncData.user.name + ' changed username to ' + newNick});

						syncData.user.name = newNick;
						syncData.room.users = tools.userlist(room, users);
						
						users[socket.id] = syncData.user;
						
						socket.emit('userSync', 'ok', syncData);
						io.to(room).emit('roomSync', 'ok', syncData.room);
					}
				}
			},
			'newColor': function(color) {
				if(tools.extract(socket.rooms, socket.id)) {
					var room = tools.extract(socket.rooms, socket.id);
					
					for(var allowed in config.defaults.colors) {
						if(color == config.defaults.colors[allowed]) {
							syncData.user.color = color;
						}
					}
				
					socket.emit('userSync', 'ok', syncData);
					io.to(room).emit('roomSync', 'ok', syncData.room);
				}
			},
			'newStatus': function(status) {
				if(tools.extract(socket.rooms, socket.id)) {
					var room = tools.extract(socket.rooms, socket.id);
		
					if(config.defaults.status.indexOf(status) != -1 && syncData.user.status != status) {
						// Set new status
						syncData.user.status = status;
						users[socket.id] = syncData.user;
		
						io.to(socket.id).emit('broadcast', {'msg': 'Status updated to \'' + syncData.user.status + '\'.', 'personal': true});
			
						// Sync user object to client
						io.to(room).emit('roomSync', 'ok', syncData.room);
					} else if(syncData.user.status == status) {
						io.to(socket.id).emit('broadcast', {'msg': 'Status already set to \'' + syncData.user.status + '\'!', 'personal': true});
					} else {
						io.to(socket.id).emit('broadcast', {'msg': 'Invalid status! Status not updated (\'' + syncData.user.status + '\').', 'personal': true});
					}
				}
			},
			'kick': function(target, auth) {
				if(auth == config.keys.mod) {
					// Look up ID in userlist if username is given
					for(var id in users) {
						if(target.toLowerCase() == users[id].name.toLowerCase()) {
							target = id;
						}
					}
				
					// io.sockets.connected no longer exists?
					/*
					if(io.sockets.connected[target]) {
						note('001', '002', syncData.user.name + ' (ID: \'' + socket.id + '\', IP: \'' + userIp + '\') kicked \'' + target + '\'');
						io.to(socket.id).emit('broadcast', {'msg': 'You kicked \'' + target + '\'.', 'personal': true});
			
						io.to(target).emit('broadcast', {'msg': 'You have been kicked. You may rejoin if you\'re not banned, but please reflect on your attitude.', 'personal': true});
						io.sockets.connected[target].disconnect();
					} else {
						io.to(socket.id).emit('broadcast', {'msg': '\'' + target + '\' does not exist!', 'personal': true});
					}
					*/
				}
			},
			'ban': function(target, auth) {
				if((auth) == config.keys.mod) {
					// Look up IP in userlist if username is given
					for(var id in users) {
						if(target.toLowerCase() == users[id].name.toLowerCase()) {
							target = users[id].ip;
						}
					}

					blacklist(function(blacklist) {
						blacklist[target] = true;
					
						note('001', '001', util.inspect(blacklist));
	
						fs.writeFile('./blacklist.json', JSON.stringify(blacklist, null, 4), function(error) {
							if(error) {
								note('001', '003', error);
							}
						});
		
						note('001', '002', syncData.user.name + ' (ID: \'' + socket.id + '\', IP: \'' + userIp + '\') banned \'' + target + '\'');
						io.to(socket.id).emit('broadcast', {'msg': 'You banned \'' + target + '\'.', 'personal': true});
			
						// io.sockets.connected no longer exists?
						/*
						// Look up IP in userlist to kick user by ID
						for(var id in users) {
							if(target == users[id].ip) {
								io.to(id).emit('broadcast', {'msg': 'You have been kicked following a ban.', 'personal': true});
								io.sockets.connected[id].disconnect();
							}
						}
						*/
					});
				}
			},
			'unban': function(target, auth) {
				if((auth) == config.keys.mod) {
					blacklist(function(blacklist) {
						blacklist[target] = false;

						fs.writeFile('./blacklist.json', JSON.stringify(blacklist, null, 4), function(error) {
							if(error) {
								note('001', '003', error);
							}
						});
	
						note('001', '002', syncData.user.name + ' (ID: \'' + socket.id + '\', IP: \'' + userIp + '\') unbanned \'' + target + '\'');
						io.to(socket.id).emit('broadcast', {'msg': 'You unbanned \'' + target + '\'.', 'personal': true});
					});
				}
			},
			// Truncate blacklist
			'flush': function(auth) {
				if((auth) == config.keys.mod) {
					blacklist = new Object();	

					fs.writeFile('./blacklist.json', JSON.stringify(blacklist, null, 4), function(error) {
						if(error) {
							note('001', '003', error);
						}
					});
	
					note('001', '002', syncData.user.name + ' (ID: \'' + socket.id + '\', IP: \'' + userIp + '\') flushed the blacklist');
					io.to(socket.id).emit('broadcast', {'msg': 'You cleared the blacklist.', 'personal': true});
				}
			},
			'help': function() {
				io.to(socket.id).emit('broadcast', {'msg': 'Application commands, prefixed with a \'<strong>/</strong>\', can be used for quick access to settings and features. They should not be confused for bot commands,<br>which request a response from an automated user (a bot) rather than the application itself. Type <strong>/commands</strong> for a list of application commands!', 'personal': true});
			},
			'commands': function() {
				var commands = {
					'/away': 'Updates your status to \'away\'. Short for \'/status away\', it can also be abbreviated to \'/afk\'.',
					'/clear': 'Removes all received messages and broadcasts from your screen (including your own).',
					'/help': 'Shows a summary about these commands.',
					'/nick [newnick]': 'Updates your username.',
					'/available': 'Updates your status (back) to \'available\'. Short for \'/status available\', it can be abbreviated further to \'/on\'.',
					'/status [status]': 'Updates your status. You can use the abbreviated commands /away and /available as well!'
				};
			
				var commandList = [];
			
				for(var command in commands) {
					commandList.push('<br><strong>' + command + '</strong>: ' + commands[command]);
				}
			
				commandList = commandList.join('');
			
				io.to(socket.id).emit('broadcast', {'msg': '<strong>The following application commands can be used</strong>: ' + commandList, 'personal': true});
			},
			'unrecognized': function(command) {
				io.to(socket.id).emit('broadcast', {'msg': 'Command \'' + command + '\' not recognized! Type <strong>/help</strong> for more information.', 'personal': true});
			}
		}
	
		socket.on('roomSync', function(clientData) {
			var rooms = clientData.room.split(',');
			
			var connect = function(room) {
				socket.join(room.id);
				syncData.user.room = room.id;
		
				if(clientData.username) {
					if(tools.validateNick(clientData.username, syncData.user.name, users) == true) {
						// Log namechange
						note('001', '001', "'" + syncData.user.name + "' changed username to '" + clientData.username + "'");
						// Set new username
						syncData.user.name = clientData.username;
					}
				}
				
				users[socket.id] = syncData.user;
				
				socket.emit('userSync', 'ok', syncData);
				
				syncData.room.id = room.id;
				syncData.room.label = room.label;
				syncData.room.users = tools.userlist(room.id, users);

				note('001', '001', '\'' + syncData.user.name + '\' joined \'' + room.id + '\' (ID: \'' + socket.id + '\', IP: \'' + userIp + '\')');
				io.to(room.id).emit('broadcast', {'msg': 'User ' + syncData.user.name + ' joined'});
				
				models.Topics
					.find({ where: { id: room.topic }})
					.then(function successCallback(topicInfo) {
						syncData.room.topic = new Object();
				
						if(topicInfo) {
							syncData.room.topic.id = topicInfo.dataValues.id;
							syncData.room.topic.title = topicInfo.dataValues.title;
							syncData.room.topic.type = topicInfo.dataValues.type;
							syncData.room.topic.url = topicInfo.dataValues.url;

							socket.emit('roomSync', 'initial', syncData.room);
							io.to(room.id).emit('roomSync', 'ok', syncData.room);
						} else {
							io.to(room.id).emit('roomSync', 'error', 'Topic does not exist in database');
						}
					}, function errorCallback(error) {
						note('002', '003', error);
						io.to(room.id).emit('roomSync', 'error', 'Database error');
					});
			}
		
			// Check if branch is specified, otherwise join primary branch for topic
			if(rooms[1]) {
				models.Rooms
					.find({ where: { id: rooms[1], topic: rooms[0] }})
					.then(function successCallback(branchedRoom) {
						if(branchedRoom) {
							connect({'id': branchedRoom.dataValues.id, 'label': branchedRoom.dataValues.label, 'topic': branchedRoom.dataValues.topic});
						} else {
							io.to(socket.id).emit('broadcast', {'msg': 'Nothing here!', 'personal': true});
						}
					}, function errorCallback(error) {
						note('002', '003', error);
					});
			} else if(rooms[0] == 'random') {
				models.Rooms
					.findAndCountAll()
					.then(function successCallback(rooms) {
						models.Rooms
							.find({ where: { id: rooms.rows[Math.floor(Math.random() * rooms.count)].dataValues.id, primary: true }})
							.then(function successCallback(randomRoom) {
								if(randomRoom) {
									connect({'id': randomRoom.dataValues.id, 'label': randomRoom.dataValues.label, 'topic': randomRoom.dataValues.topic});
								} else {
									io.to(socket.id).emit('broadcast', {'msg': 'Nothing here!', 'personal': true});
								}
							}, function errorCallback(error) {
								note('002', '003', error);
							});
					}, function errorCallback(error) {
						note('002', '003', error);
					});
			} else {
				models.Rooms
					.find({ where: { topic: rooms[0], primary: true }})
					.then(function successCallback(primaryRoom) {
						if(primaryRoom) {
							connect({'id': primaryRoom.dataValues.id, 'label': primaryRoom.dataValues.label, 'topic': primaryRoom.dataValues.topic});
						} else {
							io.to(socket.id).emit('broadcast', {'msg': 'Nothing here!', 'personal': true});
						}
					}, function errorCallback(error) {
						note('002', '003', error);
					});
			}
		});
		
		// New throttle stack for flood message control
		var throttle = new Array();
	
		var sendMessage = function(replyData) {
			var room = tools.extract(socket.rooms, socket.id);
		
			throttle.push(new Date().getTime());

			// Throttle stack limited expects filled throttle stack, thus throttling does not begin until throttle stack is filled
			if(throttle.length >= config.defaults.throttle.span) {
				// Limit throttle stack to allow for measurable time difference (track)
				throttle = throttle.slice(-config.defaults.throttle.span);
				var track = throttle[(config.defaults.throttle.span - 1)] - throttle[0];
			}
		
			if((!track || track > (config.defaults.throttle.span * config.defaults.throttle.timeout)) || replyData.throttle == config.keys.throttle) {
				io.to(room).emit('chat', replyData);
			} else {
				note('001', '002', 'User \'' + 'test' + '\' has been throttled in \'' + room + '\' (ID: ' + socket.id + ', IP: ' + userIp + ')');
				io.to(socket.id).emit('broadcast', {'msg': "You've been muted for several seconds. Please try to be less verbose!", 'personal': true});
			}
		}

		socket.on('chat', function(replySet) {
			if(typeof replySet.msg == 'string' && (replySet.msg ? replySet.msg.trim().length : 0) > 0) {
				var replyData = {
					'user': {
						'id': socket.id,
						'name': syncData.user.name,
						'color': syncData.user.color
					},
					'html': (replySet.html == config.keys.html ? true : false),
					'audio': ((replySet.audio ? replySet.audio.key : null) == config.keys.audio ? replySet.audio.id : false)
				}
			
				// Check for application command
				if(replySet.msg.trim().substring(0,1) == config.defaults.commandPrefix) {
					var command = replySet.msg.substring(1).split(' ')[0];
					var args = replySet.msg.substring(1).split(' ').slice(1);
					var arg = args.join(' ');
				
					switch(command) {
						case 'me':
							replyData['msg'] = arg;
							replyData['me'] = true;
							sendMessage(replyData);
							break;
						case 'nick':
							appCommands.newNick(args[0]);
							break;
						case 'status':
							appCommands.newStatus(args[0]);
							break;
						case 'on':
						case 'online':
						case 'available':
							appCommands.newStatus('available');
							break;
						case 'afk':
						case 'away':
							appCommands.newStatus('away');
							break;
						case 'kick':
							appCommands.kick(args[0], args[1]);
							break;
						case 'ban':
							appCommands.ban(args[0], args[1]);
							break;
						case 'unban':
							appCommands.unban(args[0], args[1]);
							break;
						case 'flush':
							appCommands.flush(args[0]);
							break;
						case 'help':
							appCommands.help();
							break;
						case 'commands':
							appCommands.commands();
							break;
						default:
							appCommands.unrecognized(command);
					}
				} else {
					replyData['msg'] = replySet.msg.substring(0,config.defaults.messages.maxLength);
					sendMessage(replyData);
				}
			}
		});

		socket.on('config', function(setting, data) {
			switch(setting) {
				case 'color':
					appCommands.newColor(data);
					break;
				case 'nick':
					appCommands.newNick(data);
					break;
			}
		});

	
		socket.on('disconnect', function() {
			note('003', '001', 'Socket connection ended (chat, \'' + userIp + '\')');
		
			// Remove user from users list object
			delete users[socket.id];
			syncData.room.users = tools.userlist(syncData.room.id, users);
			
			// Log and broadcast leave
			note('001', '001', '\'' + syncData.user.name + '\' left \'' + syncData.room.id + '\' (ID: \'' + socket.id + '\', IP: \'' + userIp + '\')');
			io.to(syncData.room.id).emit('broadcast', {'msg': 'User ' + syncData.user.name + ' left'});
		
			io.to(syncData.room.id).emit('roomSync', 'ok', syncData.room);
		});
	});
}
