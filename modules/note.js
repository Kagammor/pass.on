// Load 3rd party modules
var clc = require('cli-color');
var moment = require('moment');

// Console logging (formatted and colorized)
module.exports = function note(source, type, msg, flag) {
	// Assign source numbers to codes
	var sources = {'001': 'NODEJS', '002': 'SQLIZE', '003': 'SOCKIO', '004': 'UPLOAD', '101': 'FOOBOT'};

	// Assign debug numbers to codes
	var types = {
	'001': 'LOGS', '002': 'WARN', '003': 'ERRO',
	'101': 'CONN', '102': 'DISC',
	'201': 'JOIN', '202': 'LEFT', '203': 'MESG'};

	// Highlight flagged messages
	if(flag) {
		var msg = clc.blueBright.bold(msg);
	}
	// Highlight error messages
	if(type == '002') {
		var type = clc.yellowBright('[' + types[type] + ']');
		var msg = clc.yellowBright(msg);
	} else if(type == '003') {
		var type = clc.redBright('[' + types[type] + ']');
		var msg = clc.redBright(msg);
	} else {
		if(type == '203') {
			var msg = clc.cyan(msg);
		}

		var type = clc.blue('[' + types[type] + ']');
	}

	// Log debug to console
	console.log(clc.green('[' + moment().format("YYYY-MM-DD HH:mm:ss") + ']') + clc.magenta('[' + sources[source] + ']') + type + ' ' + msg);
}
