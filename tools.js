module.exports = function(config, note, util, uuid, prove) {
	return {
		// Socket.io joins a room based on the user's ID. This separate the joined room from the user's own ID-based room.
		'extract': function(rooms, id) {
			for(var room in rooms) {
				if(rooms[room] != id) {
					var extracted = rooms[room];
				}
			}
	
			return extracted;
		},
		// Generate random color value (0-255, range 100-200 for pastel palette)
		'hexgen': function() {
			return Math.floor(Math.random() * 100) + 100;
		},
		'id': function(type) {
			switch(type) {
				case 'topic':
					return uuid().split('-').slice(0,1).toString();
					break;
				case 'room':
					return uuid().split('-').slice(0,1).toString();
					break;
				case 'file':
					return new Date().getTime() + '-' + uuid().split('-').join('');
					break;
				default:
					return uuid();
			}
		},
		// Check if new username is valid and free
		'validateNick': function(newNick, currentNick, allUsers) {
			var usernames = new Array();
	
			for(var user in allUsers) {
				usernames.push(allUsers[user].name);
			}

			var schema = prove()
				.alphaNumeric().annotate({'alphaNumeric': '\'' + newNick + '\' contains invalid characters!'})
				.length(config.defaults.username.minLength, config.defaults.username.maxLength).annotate({'length': '\'' + newNick + '\' has an invalid length!'})
				.not.equals(currentNick).annotate({'not.equals': '\'' + newNick + '\' is already used by you!'})
				.not.conflict(usernames).annotate({'not.conflict': '\'' + newNick + '\' is already in use!'});
	
			return schema.test(newNick);
		},
		// Format user objects into array
		'userlist': function(room, allUsers) {
			var userlist = new Object();
			
			for(var user in allUsers) {
				if(allUsers[user].room == room) {
					userlist[user] = allUsers[user];
				}
			}
			
			return userlist;
		}
	}
}
