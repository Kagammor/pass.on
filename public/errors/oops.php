<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1,user-scalable=no">
	
		<title>Error - Pass.on</title>
		
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css">
		<link href="/assets/css/oops.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<div class="oops">
			<h1>Oops!</h1>
			
			<p><span>Looks like we've slipped.</span></p>
			
			<p><a href="http://passon.memody.com/">Here's a way back!</a></p>
			
			<a href="http://en.wikipedia.org/wiki/List_of_HTTP_status_codes" target="_blank" class="error" id="error"></a>
			
			<script src="/assets/js/jquery.js"></script>
			
			<script>
				var req = new XMLHttpRequest();
				req.open('GET', document.location, true);
				req.send(null);
				
				req.onreadystatechange = function() {
					document.title = document.title.replace('Error', req.status);
					
					$('#error').text(req.status + ' - ' + req.statusText);
				}
			</script>
		</div>
	</body>
</html>
